package ru.tsc.ichaplygina.taskmanager;

public class TaskManager {

    public static void main(final String[] args) {
        showWelcome();
        run(args);
    }

    public static void run(final String[] params) {
        if (params == null || params.length == 0) return;
        switch (params[0]) {
            case TerminalConst.CMD_ABOUT:
                showAbout();
                break;
            case TerminalConst.CMD_VERSION:
                showVersion();
                break;
            case TerminalConst.CMD_HELP:
                showHelp();
                break;
            default:
                showUnknown(params[0]);
        }
    }

    public static void showWelcome() {
        System.out.println("\n*** WELCOME TO THE ULTIMATE TASK MANAGER ***\n");
    }

    public static void showVersion() {
        System.out.println("Version: 0.1.0");
    }

    public static void showAbout() {
        System.out.println("Developed by:");
        System.out.println("Irina Chaplygina,");
        System.out.println("Technoserv Consulting,");
        System.out.println("ichaplygina@tsconsulting.com");
    }

    public static void showHelp() {
        System.out.println("Available commands:");
        System.out.println(TerminalConst.CMD_VERSION + " - show version info;");
        System.out.println(TerminalConst.CMD_ABOUT + " - show developer info;");
        System.out.println(TerminalConst.CMD_HELP + " - show this message.");
    }

    public static void showUnknown(final String arg) {
        System.out.println("Unknown argument " + arg);
    }

}
