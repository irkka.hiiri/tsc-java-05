# Task Manager 

Training project.  
A simple console application for managing task lists.

## TECH STACK

Java Core

## SOFTWARE

* JDK 1.8
* Maven 3.6.3
* Windows 10 64-bit

## HARDWARE

* Core i7-4790 or Ryzen 3 3200G.
* GTX 1060 6GB, GTX 1660 Super (or R9 Fury)
* 12GB RAM.
* 6GB VRAM.
* 70GB SSD storage.

## BUILD 

```
mvn clean install
```

## RUN   

```
java -jar ./target/task-manager.jar
```

## DEVELOPER 

Irina Chaplygina  
Technoserv Consulting  
ichaplygina@tsconsulting.com

## SCREENSHOTS

Build:  
![mvn clean install](https://gitlab.com/irkka.hiiri/tsc-java-misc/-/raw/master/screenshots/tsc-java-05/1_mvn_clean_install.png)
  
Run:  
![!screenshot of the application running from jar](https://gitlab.com/irkka.hiiri/tsc-java-misc/-/raw/master/screenshots/tsc-java-05/2_jar.png)
